<?php
declare(strict_types=1);


namespace ConfigValidator\Test;


use ConfigValidator\Validator;

final class TestValidator extends Validator
{
    /**
     * @var iterable
     */
    private $requirements;

    /**
     * TestValidator constructor.
     * @param iterable $config
     * @param iterable $requirements
     */
    public function __construct(iterable $config, iterable $requirements)
    {
        $this->requirements = $requirements;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    protected function getRequirements(): iterable
    {
        return $this->requirements;
    }
}
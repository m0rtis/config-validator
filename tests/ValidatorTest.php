<?php
declare(strict_types=1);


namespace ConfigValidator\Test;


use ConfigValidator\Type;
use ConfigValidator\ValidationException;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;

final class ValidatorTest extends TestCase
{
    private $reqs = [
        'testInt' => Type::INT,
        'testString' => Type::STRING,
        'testDataset' => [
            Type::DATASET => [
                'datasetKeyOne' => Type::STRING,
                'datasetKeyTwo' => Type::INT,
                'datasetSubArray' => [
                    Type::ARRAY => [
                        'datasetSubArrayKey' => Type::BOOL
                    ]
                ]
            ]
        ],
        'testArray' => [
            Type::ARRAY => [
                'testSubEmail' => Type::EMAIL,
                'testClassType' => \Exception::class,
                'testIfaceType' => Test::class,
                'testIterable' => [
                    Type::ITERABLE => [
                        'testAlternatives' => [
                            Type::STRING,
                            Type::ARRAY => [
                                'subSubSubKey' => [
                                    Type::REGEXP => '/.*test regexp.*/'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'testFile' => Type::FILE,
        'testBool' => Type::BOOL,
        'testDir' => Type::DIR,
        'testFloat' => Type::FLOAT,
        'testUrl' => Type::URL,
        'testCallable' => Type::CALLABLE
    ];

    private $conf;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->conf = [
            'testInt' => 257,
            'testString' => 'string',
            'testDataset' => [
                [
                    'datasetKeyOne' => 'some string',
                    'datasetKeyTwo' => 555,
                    'datasetSubArray' => [
                        'datasetSubArrayKey' => false
                    ]
                ],
                [
                    'datasetKeyOne' => 'another string',
                    'datasetKeyTwo' => 777,
                    'datasetSubArray' => [
                        'datasetSubArrayKey' => true
                    ]
                ]
            ],
            'testArray' => [
                'testSubEmail' => 'mail@m0rtis.ru',
                'testClassType' => new ValidationException([]),
                'testIfaceType' => $this,
                'testIterable' => [
                    'testAlternatives' => [
                        'subSubSubKey' => 'try to test regexp today'
                    ]
                ]
            ],
            'testFile' => __FILE__,
            'testBool' => true,
            'testDir' => __DIR__,
            'testFloat' => 3.1415,
            'testUrl' => 'https://packagist.org/m0rtis/config-validator',
            'testCallable' => '\str_replace'
        ];
    }


    public function testSuccess(): void
    {
        $config = new TestValidator($this->conf, $this->reqs);
        $config = \iterator_to_array($config);
        $this->assertSame($this->conf, $config);
    }

    public function testMissedKey(): void
    {
        $conf = [];
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessageRegExp('/.*required key.*/');
        new TestValidator($conf, $this->reqs);
    }

    public function testInvalidString(): void
    {
        $this->expectExceptionMessageRegExp('/.*should be string.*/');
        $conf  = $this->conf;
        $conf['testString'] = new \stdClass();
        new TestValidator($conf, $this->reqs);
    }

    public function testRegexp(): void
    {
        $conf = $this->conf;
        $conf['testArray']['testIterable']['testAlternatives']['subSubSubKey'] = 'fail string';
        $this->expectExceptionMessageRegExp('/.*regular expression.*/');
        new TestValidator($conf, $this->reqs);
    }

    public function testAlternatives(): void
    {
        $conf = $this->conf;
        $conf['testArray']['testIterable']['testAlternatives'] = 'string';
        $config = new TestValidator($conf, $this->reqs);
        $config = \iterator_to_array($config);
        $this->assertSame($conf, $config);

        $conf['testArray']['testIterable']['testAlternatives'] = false;
        $this->expectExceptionMessageRegExp('/.*value should be.*/');
        new TestValidator($conf, $this->reqs);
    }
}
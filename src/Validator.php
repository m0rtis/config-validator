<?php
declare(strict_types=1);


namespace ConfigValidator;


/**
 * Class Validator
 * @package ConfigValidator
 */
abstract class Validator extends \ArrayObject
{
    /**
     * BaseConfigValidator constructor.
     * @param iterable $config
     * @throws ValidationException
     */
    public function __construct(iterable $config)
    {
        $config = $this->validate($config);
        parent::__construct($config);
    }

    /**
     * @param iterable $config
     * @return iterable
     * @throws ValidationException
     */
    protected function validate(iterable $config): iterable {
        $constraints = $this->getRequirements();
        $exceptions = $this->matchRequirements($constraints, $config);
        if (\count($exceptions) > 0) {
            throw new ValidationException($exceptions);
        }
        return $config;
    }

    /**
     * @return array
     */
    abstract protected function getRequirements(): iterable;

    /**
     * @param iterable $requirements
     * @param iterable $config
     * @return array
     */
    private function matchRequirements(iterable $requirements, iterable $config): array
    {
        $exceptions = [];
        foreach ($requirements as $key => $node) {
            if (!isset($config[$key])) {
                $exceptions[] = [new \InvalidArgumentException("Missing required key '$key'")];
                continue;
            }
            if (\is_iterable($node)) {
                if (\is_iterable($config[$key])) {
                    if (isset($node[Type::ARRAY]) || isset($node[Type::ITERABLE])) {
                        $subNode = $node[Type::ARRAY] ?? $node[Type::ITERABLE];
                        $exceptions[] = $this->matchRequirements($subNode, $config[$key]);
                        continue;
                    }
                    if (isset($node[Type::DATASET])) {
                        foreach ($config[$key] as $item) {
                            $exceptions[] = $this->matchRequirements($node[Type::DATASET], $item);
                        }
                        continue;
                    }
                }
                if (isset($node[Type::REGEXP])) {
                    if (!$this->matchRegexp($node[Type::REGEXP], $config[$key])) {
                        $exceptions[] = [new \InvalidArgumentException(
                            sprintf(
                                "Key %s should matches the regular expression: '%s'",
                                $key,
                                $node[Type::REGEXP]
                            )
                        )];
                    }
                    continue;
                }
                \array_walk($node, function(& $item, $key) {
                    if (\is_iterable($item)) {
                        $item = $key;
                    }
                });
                if (!$this->matchAlternatives($node, $config[$key])) {
                    $exceptions[] = [new \InvalidArgumentException(sprintf(
                        'The type of key %s value should be %s.',
                        $key,
                        implode(' or ', $node)
                    ))];
                }
            } elseif (!$this->matchType($node, $config[$key])) {
                $exceptions[] = [new \InvalidArgumentException(sprintf(
                    'The type of key %s value should be %s.',
                    $key,
                    $node
                ))];
            }
        }
        if (\count($exceptions) > 0) {
            $exceptions = \array_merge(...$exceptions);
            $exceptions = \array_filter($exceptions);
        }
        return $exceptions;
    }

    /**
     * @param string $pattern
     * @param string $value
     * @return bool
     */
    protected function matchRegexp(string $pattern, string $value): bool
    {
        return (bool)\preg_match($pattern, $value);
    }

    /**
     * @param iterable $alternatives
     * @param $configNode
     * @return bool
     */
    private function matchAlternatives(iterable $alternatives, $configNode): bool
    {
        $resultArray = [];
        foreach ($alternatives as $type => $alternative) {
            $resultArray[] = $this->matchType($alternative, $configNode);
        }
        $resultArray = \array_filter($resultArray);
        return \count($resultArray) !== 0 ;
    }

    /**
     * @param string $type
     * @param $item
     * @return bool
     */
    private function matchType(string $type, $item): bool
    {
        $itemType = \is_object($item) ? \get_class($item) : \gettype($item);
        $result = $type === $itemType;
        if (!$result
            && \is_object($item)
            && (\class_exists($type) || \interface_exists($type))
        ) {
            $result = $item instanceof $type;
        }
        $method = 'match'.\ucfirst($type);
        if (!$result && \method_exists($this, $method)) {
            $result = $this->$method($item);
        }
        return $result;
    }

    protected function matchCallable($value): bool
    {
        return \is_callable($value);
    }

    /**
     * @param string $email
     * @return bool
     */
    protected function matchEmail(string $email): bool
    {
        return (bool)\filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $url
     * @return bool
     */
    protected function matchUrl(string $url): bool
    {
        return (bool)\filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * @param string $filename
     * @return bool
     */
    protected function matchFile(string $filename): bool
    {
        return (new \SplFileInfo($filename))->isFile();
    }

    /**
     * @param string $dirname
     * @return bool
     */
    protected function matchDir(string $dirname): bool
    {
        return (new \SplFileInfo($dirname))->isDir();
    }
}

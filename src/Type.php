<?php
declare(strict_types=1);


namespace ConfigValidator;


final class Type
{
    public const STRING = 'string';
    public const BOOL = 'boolean';
    public const ARRAY = 'array';
    public const ITERABLE = 'iterable';
    public const INT = 'integer';
    public const FLOAT = 'double';
    public const CALLABLE = 'callable';
    public const DATASET = 'dataset';
    public const REGEXP = 'regexp';
    public const EMAIL = 'email';
    public const URL = 'url';
    public const FILE = 'file';
    public const DIR = 'dir';
}
<?php
declare(strict_types=1);


namespace ConfigValidator;


/**
 * Class ValidationException
 * @package ConfigValidator
 */
final class ValidationException extends \DomainException
{
    /**
     * @var \Exception[]
     */
    private $exceptions;
    /**
     * @var array
     */
    private $messages;

    /**
     * @return \Exception[]
     */
    public function getExceptions(): array
    {
        return $this->exceptions;
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * ValidationException constructor.
     * @param \Exception[] $exceptions
     */
    public function __construct(array $exceptions)
    {
        $this->exceptions = $exceptions;
        $messages = [];
        foreach ($exceptions as $exception) {
            if (!$exception instanceof \Exception) {
                continue;
            }
            $messages[] = $exception->getMessage();
        }
        $this->messages = $messages;
        parent::__construct(implode(PHP_EOL,$messages));
    }
}